
If you are going to clone this codebase:


1. Copy the ssh link from the repo
2. run this command in git bash/terminal:
		git clone <ssh link>
3. The code base will be copied to your local machine
4. Make sure you are inside the folder/root directory (where package,json is located), then run this command:
		npm install
5. Create .env file from root directory and paste your mongodb connection string
6. Run the command in gitbash:
	nodemon index.js

